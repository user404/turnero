<?php

namespace IPDUV\Event\Listener;


use Doctrine\Common\EventArgs;
use \ZMQContext;
use \ZMQ;

use Symfony\Component\DependencyInjection\ContainerInterface;
use IPDUV\TurnadorBundle\Entity\Turno;
   //   IPDUV\TurnadorBundle\Entity\Turno
class ProyectoListener
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function postUpdate(EventArgs $args)
    {
        $responder = new ZMQSocket($context, ZMQ::SOCKET_REP);
     //   $cola = new ZMQContext();//new ZMQContext(), ZMQ::SOCKET_REQ);
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        if ($entity instanceof Turno) {
            $usuario = 131;
            $entity->setNumero($usuario);
        //    $this->container->get('security.context')->getToken()->getUser();
            $entityManager->flush();
        }
    }
}
