<?php

namespace IPDUV\TurnadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TurnadorBundle\Entity\Consulta;
use IPDUV\TurnadorBundle\Form\ConsultaType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Consulta controller.
 *
 * @Route("/consulta")
 */
class ConsultaController extends Controller
{
    /**
     * Lists all Fase entities.
     *
     * @Route("/traer-consultas/{id}", name="traer_consultas", options={"expose"=true})
     * @Method("GET")
     * @Template()
     */
    public function traerConsultasAction($id)
    {
        $em = $this->getDoctrine()->getManager();

      //  $fases = $em->getRepository('IPDUVTierrasBundle:Fase')->findAllFases($id);
     $exp = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);


        if(count($exp->getConsultas()) != 0){

            foreach ($exp->getConsultas() as $fases) {
                 $ho = array(
                    'Id' => $fases->getId(),
                    'Fecha' => $fases->getFecha(),
                    'Asunto'=>$fases->getAsunto(),
                    'Detalle' =>$fases->getDetalle(),
                    );
                 $array[] = $ho; 
            }
         }
         else{

            $array = array();
        }

   $array2 = array( "data" => $array );
//var_dump($array2);

    $response = new JsonResponse();


    $response->setData($array2);

    return $response;
       
    }



    /**
     * @Route("/showajax/{id}", name="show_consulta_ajax", options={"expose"=true})
     * @Method("GET")
     */
    public function showAjaxAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

        if($entity)
        {
            $array = array(
            'valor' => "true",
            );
        }
        else
        {
            $array = array(
            'valor' => "false",
            );
        }
         
        $response = new JsonResponse();
        $response->setData($array);        
        return $response;
    }

    /**
     * @Route("/editajax/{id}", name="edit_consulta_ajax", options={"expose"=true})
     * @Method("PUT")
     */
    public function editAjaxAction($id)
    {

        $resultado=false;
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

        //throw $this->createNotFoundException($entity->getNumero());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $form = $this->createEditForm($entity);

        $form->handleRequest($request);

        //  throw $this->createNotFoundException($entity->getLetra());
        if ($form->isValid()) {
            $em->flush();

               $resultado=true;
        }

        $array = array(
            'resultado' => $resultado,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;   
    
    }

    /**
     * @Route("/postajaxbeneficiario/{id}", name="post_ajax_beneficiario", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction1($id) {

        $request = $this->getRequest();

        $entity = new Consulta();
        $resultado = false;


        $em = $this->getDoctrine()->getManager();
        
        $terreno = new Terreno();
        $terreno = $em->getRepository('IPDUVTierrasBundle:Terreno')->find($id);

        $entity->setTerreno($terreno);
        $entity->setDireccion($request->request->get('direccion'));
        $entity->setNombre($request->request->get('nombre'));
        $entity->setApellido($request->request->get('apellido'));
        $entity->setDocumento($request->request->get('documento'));
        $entity->setEmail($request->request->get('email'));
        $entity->setTelefono($request->request->get('telefono'));
        $entity->setMz($request->request->get('mz'));
        $entity->setPc($request->request->get('pc'));
        
             //throw $this->createNotFoundException($terreno->getLineaRestriccion());

        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
         
         $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
            
        
        $array = array(
            'Mensaje' => 'El beneficiario se cargo exitosamente !!'
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }


    /**
     * @Route("/postajax/", name="post_consulta_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction() {

        $request = $this->getRequest();

        $entity = new Consulta();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
            
        //var_dump($entity);
        //die;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }

    /**
     * Lists all Consulta entities.
     *
     * @Route("/", name="consulta")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Consulta')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Consulta entity.
     *
     * @Route("/", name="consulta_create")
     * @Method("POST")
     * @Template("IPDUVTurnadorBundle:Consulta:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Consulta();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('consulta_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Consulta entity.
     *
     * @param Consulta $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Consulta $entity)
    {
        $form = $this->createForm(new ConsultaType(), $entity, array(
            'action' => $this->generateUrl('consulta_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Consulta entity.
     *
     * @Route("/new", name="consulta_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Consulta();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Consulta entity.
     *
     * @Route("/{id}", name="consulta_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consulta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Consulta entity.
     *
     * @Route("/{id}/edit", name="consulta_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

//        $form->add('submit', 'submit', array('label' => ' Agregar', 'attr' => array('class' => 'btn btn-primary glyphicon glyphicon-plus'

  //      $tiposFase = $em->getRepository('IPDUVTierrasBundle:TipoFase')->findAll();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consulta entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Consulta entity.
    *
    * @param Consulta $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Consulta $entity)
    {
        $form = $this->createForm(new ConsultaType(), $entity, array(
            'action' => $this->generateUrl('consulta_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->add('ajax_id', 'hidden', array('data' => $entity->getId()));
        return $form;
    }
    /**
     * Edits an existing Consulta entity.
     *
     * @Route("/{id}", name="consulta_update")
     * @Method("PUT")
     * @Template("IPDUVTurnadorBundle:Consulta:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Consulta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('consulta_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Consulta entity.
     *
     * @Route("/{id}", name="consulta_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTurnadorBundle:Consulta')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Consulta entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('consulta'));
    }

    /**
     * Creates a form to delete a Consulta entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('consulta_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
