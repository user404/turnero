<?php

namespace IPDUV\TurnadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TurnadorBundle\Entity\Cliente;
use IPDUV\TurnadorBundle\Form\ClienteType;
use Symfony\Component\HttpFoundation\JsonResponse;

use IPDUV\TurnadorBundle\Entity\Consulta;
use IPDUV\TurnadorBundle\Form\ConsultaType;


/**
 * Cliente controller.
 *
 * @Route("/cliente")
 */
class ClienteController extends Controller
{

    /**
     * @Route("/showajaxcliente/{dni}", name="show_ajax_cliente", options={"expose"=true})
     * @Method("GET")
     */
    public function showAjaxAction($dni) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($dni);

        $array = array();

        //throw $this->createNotFoundException($entity->getId());
        if($entity)
        {
            $array = array(
            'valor' => "true",
            );
        }
        else
        {
            $array = array(
            'valor' => "false",
            );
        }

         $response = new JsonResponse();
         // $response->setData($jsonContent);       
          $response->setData($array);         
         return $response;
    }


    /**
     * @Route("/showajax/{id}", name="show_cliente_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function showAjax1Action($id) {
        


        throw $this->createNotFoundException($entity->getId());
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

        throw $this->createNotFoundException($entity->getId());
        if($entity)
        {
            $array = array(
            'valor' => "true",
            );
        }
        else
        {
            $array = array(
            'valor' => "false",
            );
        }
         
        $response = new JsonResponse();
        $response->setData($array);        
        return $response;
    }

    /**
     * @Route("/editajax/{id}", name="edit_cliente_ajax", options={"expose"=true})
     * @Method("PUT")
     */
    public function editAjaxAction($id)
    {

        $resultado=false;
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

        //throw $this->createNotFoundException($entity->getNumero());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferente entity.');
        }

        $form = $this->createEditForm($entity);

        $form->handleRequest($request);

        //  throw $this->createNotFoundException($entity->getLetra());
        if ($form->isValid()) {
            $em->flush();

               $resultado=true;
        }

        $array = array(
            'resultado' => $resultado,
        );

        $response = new JsonResponse();
        $response->setData($array);

        return $response;   
    
    }

    /**
     * @Route("/postajax/", name="post_cliente_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction() {
        $request = $this->getRequest();

        $entity = new Cliente();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
            
        //var_dump($entity);
        //die;


        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }

    /**
     * Lists all Cliente entities.
     *
     * @Route("/", name="cliente")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Cliente')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Cliente entity.
     *
     * @Route("/", name="cliente_create")
     * @Method("POST")
     * @Template("IPDUVTurnadorBundle:Cliente:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Cliente();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('cliente_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Cliente entity.
     *
     * @param Cliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('cliente_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }



    /**
     * Displays a form to create a new Cliente entity.
     *
     * @Route("/new", name="cliente_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Cliente();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Cliente entity.
     *
     * @Route("/{id}", name="cliente_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Cliente entity.
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Cliente entity.
    *
    * @param Cliente $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Cliente $entity)
    {
        $form = $this->createForm(new ClienteType(), $entity, array(
            'action' => $this->generateUrl('cliente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Cliente entity.
     *
     * @Route("/{id}", name="cliente_update")
     * @Method("PUT")
     * @Template("IPDUVTurnadorBundle:Cliente:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cliente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('cliente_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Cliente entity.
     *
     * @Route("/{id}", name="cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTurnadorBundle:Cliente')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cliente entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('cliente'));
    }

    /**
     * Creates a form to delete a Cliente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cliente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
