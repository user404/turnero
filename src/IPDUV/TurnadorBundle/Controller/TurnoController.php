<?php

namespace IPDUV\TurnadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TurnadorBundle\Entity\Turno;
use IPDUV\TurnadorBundle\Form\TurnoType;

use IPDUV\TurnadorBundle\Entity\Box;
use IPDUV\TurnadorBundle\Form\BoxType;

use IPDUV\TurnadorBundle\Entity\Tipo_Box;
use IPDUV\TurnadorBundle\Form\Tipo_BoxType;

use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Turno controller.
 *
 * @Route("/turno")
 */
class TurnoController extends Controller
{

    /**
     * @Route("/editar-turno/", name="editar_turno", options={"expose"=true})
     * @Method("POST")
     */
    public function editarTurnoAction(Request $request) {
        $entity = new Turno();
       
        $em = $this->getDoctrine()->getManager();
        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);


        $em = $this->getDoctrine()->getManager();

        $id_box = $request->request->get('numero_box');
        

        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($id_box);


        $id_tipo_box = $request->request->get('tipo-box');

        $tipoBox_var = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($id_tipo_box);

        $turno->setTipo($tipoBox_var->getLetra()."");

//var_dump($turno->getTipo());die;

        $turno->setBox($box);

        $turno->setNumero4('');
        $turno->setBox4('');
        $turno->setTipo4('');

        $turno->setNumero3('');
        $turno->setBox3('');
        $turno->setTipo3('');

        $turno->setNumero2('');
        $turno->setBox2('');
        $turno->setTipo2('');

        $turno->setNumero1('');
        $turno->setBox1('');
        $turno->setTipo1('');

        $numero = $request->request->get('numero');

        $turno->setNumero($numero);

        $em = $this->getDoctrine()->getManager();
        $em->persist($turno);
        $em->flush();

        $array = array(
            'turno' => $turno->getNumero(),
            'box' => $box->getNumero() . ' - ' . $turno->getTipo()."",
            
            'box1' => $turno->getBox1(),
            'turno1' => '',

            'box2' => $turno->getBox2(),
            'turno2' => '',

            'box3' => $turno->getBox3(),
            'turno3' => '',

            'box4' => $turno->getBox4(),
            'turno4' => '',

            'Mensaje' => 'Modificacion exitosa',
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;
        
        }


    /**
     * Lists all Curso entities.
     *
     * @Route("/traer-turno/", name="traer_turno", options={"expose"=true})
     * @Method("GET")
     */
    public function traerBeneficiariosAction()
    { 
        $em = $this->getDoctrine()->getManager();

        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);

        if(count($turno) != 0){
          //  foreach ($proga->getBeneficiarios() as $postu) {
                 $ho = array(
                                    'id' => $turno->getId(),
                                    'box' => $turno->getBox()->getId(),
                                  //  'area_id'=>$turno->getNombre() . ' ' . $turno->getApellido(),  
                                    'numero' => $turno->getNumero(),                             
                                    'numero1' => $turno->getNumero1(), 
                                    'numero2' => $turno->getNumero2(), 
                                    'numero3' => $turno->getNumero3(), 
                                    'numero4' => $turno->getNumero4(), 
                                    'box1' => $turno->getBox(),
                                    'box2' => $turno->getBox(),
                                    'box3' => $turno->getBox(),
                                    'box4' => $turno->getBox(),
                                    'tipo1' => $turno->getTipo1(),
                                    'tipo2' => $turno->getTipo2(),
                                    'tipo3' => $turno->getTipo3(),
                                    'tipo4' => $turno->getTipo4(),

                                  );
                 $array[] = $ho;
            //}
        }
        else{
            $array = array();
        }
       
        $array2 = array( "data" => $array );
        
        $response = new JsonResponse();
        
        
        $response->setData($array2);

        return $response;
    }

    
    /**
     * @Route("/showajax/", name="show_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function showAjaxAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);

//        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($entity->getBox()->getNumero());
// throw $this->createNotFoundException($box->getNumero());
// throw $this->createNotFoundException($entity->getBox()->getNumero());
        $array = array(
            'box' => $entity->getBox()->getNumero(), //. ' - ' . $entity->getTipo(),
            'turno' => $entity->getNumero(),
            'tipo' => $entity->getTipo(),

            'box1' => $entity->getBox1() . ' - ' . $entity->getTipo1(),
            'turno1' => $entity->getNumero1(),
          //  'tipo1' => $entity->getTipo1(),


            'box2' => $entity->getBox2() . ' - ' . $entity->getTipo2(),
            'turno2' => $entity->getNumero2(),
       //     'tipo2' => $entity->getTipo2(),

            'box3' => $entity->getBox3(). ' - ' . $entity->getTipo3(),
            'turno3' => $entity->getNumero3(),
     //       'tipo3' => $entity->getTipo3(),

            'box4' => $entity->getBox4(). ' - ' . $entity->getTipo4(),
            'turno4' => $entity->getNumero4(),
         //   'tipo4' => $entity->getTipo4(),

           // 'box' => $entity->getBox()->getNumero(),
          //  'turno' => $entity->getNumero(),
          //  'tipo' => $entity->getTipo(),

         //   'anterior' => $entity->getNumero(),
         //   'boxAnterior' => $entity->getLetra(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);        
        return $response;
    }
    
    

    /**
     * @Route("/postajax/", name="post_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction() {
        $request = $this->getRequest();
        $entity = new Turno();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        //var_dump($entity);
        //die;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }
    
    /**
     * @Route("/editajax/", name="edit_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAction(Request $request) {

        $entity = new Turno();
        $resultado = false;

        $em = $this->getDoctrine()->getManager();

        $hola = $request->request->get('tipo-box');

        $tipoBox_var = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($hola);

        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getId());
        //$box->setTipoBox($tipoBox_var);

        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);
        
        $numero_turno = 0;
        //$tipoBox_var  para saber que ya esta
        //el box que llama tambien ya esta
        //TURNOS DE REUNION
       // var_dump($hola);die;
        if($hola == 4){
        $numero_turno = $turno->getNumero4() + 1;
        $turno->setNumero4($numero_turno);
        $turno->setBox4($box->getNumero()."");
        $turno->setTipo4($tipoBox_var->getLetra()."");
        }else
        //TURNOS DE REGULARIZACION
        if($hola == 3){
        $numero_turno = $turno->getNumero3() + 1;
        $turno->setNumero3($numero_turno);
        $turno->setBox3($box->getNumero()."");
        $turno->setTipo3($tipoBox_var->getLetra()."");
        }else

        //TURNOS DE ADJUDICACCION
        if($hola == 2){
        $numero_turno = $turno->getNumero2() + 1;
        $turno->setNumero2($numero_turno);
        $turno->setBox2($box->getNumero()."");
        $turno->setTipo2($tipoBox_var->getLetra()."");
        }else

        //TURNOS DE INSCRIPCIONES
        if($hola == 1){
        $numero_turno = $turno->getNumero1() + 1;
        $turno->setNumero1($numero_turno);
        $turno->setBox1($box->getNumero()."");
        $turno->setTipo1($tipoBox_var->getLetra()."");
        }

        $turno->setBox($box);
        $turno->setNumero($numero_turno);
        $turno->setTipo($tipoBox_var->getLetra()."");
     //   var_dump($turno->getTipo());die;
   //     $turno->setTipoBox($tipoBox_var);
      
            if($numero_turno >= 100)
            {
                $turno->setNumero(0);
                $turno->setBox1(0);
                $turno->setNumero1(0);
                $turno->setBox2(0);
                $turno->setNumero2(0);
                $turno->setBox3(0);
                $turno->setNumero3(0);
                $turno->setBox4(0);
                $turno->setNumero4(0);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($turno);
            $em->flush();
            
  //      }
        $array = array(
            'turno' => $numero_turno,
            'box' => $box->getNumero() . ' - ' . $turno->getTipo()."",
            
            'box1' => $turno->getBox1(),
            'turno1' => $turno->getNumero1() . ' - ' . $turno->getTipo1(),
//            'tipo1' => $turno->getTipo1(),

            'box2' => $turno->getBox2(),
            'turno2' => $turno->getNumero2() . ' - ' . $turno->getTipo2(),
         //   'tipo2' => $turno->getTipo2(),

            'box3' => $turno->getBox3(),
            'turno3' => $turno->getNumero3() . ' - ' . $turno->getTipo3(),
         //   'tipo3' => $turno->getTipo3(),

            'box4' => $turno->getBox4(),
            'turno4' => $turno->getNumero4() . ' - ' . $turno->getTipo4(),
           // 'tipo4' => $turno->getTipo4(),
          //  'box' => $turno->getLetra(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;
        
        }

    /**
     * @Route("/resetajax/", name="reset_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function resetAjaxAction(Request $request) {
        $entity = new Turno();
       
        $em = $this->getDoctrine()->getManager();
        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);


        $em = $this->getDoctrine()->getManager();

        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getId());


        $hola = $request->request->get('foo');

        $tipoBox_var = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($hola);


        $turno->setTipo($tipoBox_var->getLetra()."");

       // var_dump($turno->getTipo());die;

        
        $numero_turno = 1;
        //$tipoBox_var  para saber que ya esta
        //el box que llama tambien ya esta
        //TURNOS DE REUNION
       // var_dump($hola);die;
        if($hola == 4){

        $turno->setNumero4($numero_turno);
        $turno->setBox4($box->getNumero()."");
        $turno->setTipo4($tipoBox_var->getLetra()."");
        }else
        {
          $turno->setNumero4('');
          $turno->setBox4('');
          $turno->setTipo4('');
        }
        //TURNOS DE REGULARIZACION
        if($hola == 3){

        $turno->setNumero3($numero_turno);
        $turno->setBox3($box->getNumero()."");
        $turno->setTipo3($tipoBox_var->getLetra()."");
        }else
        {
            $turno->setNumero3('');
           $turno->setBox3('');
          $turno->setTipo3('');
          
        }

        //TURNOS DE ADJUDICACCION
        if($hola == 2){

        $turno->setNumero2($numero_turno);
        $turno->setBox2($box->getNumero()."");
        $turno->setTipo2($tipoBox_var->getLetra()."");
        }else
        {
          $turno->setNumero2('');
        $turno->setBox2('');
        $turno->setTipo2('');
        }

        //TURNOS DE INSCRIPCIONES
        if($hola == 1){

        $turno->setNumero1($numero_turno);
        $turno->setBox1($box->getNumero()."");
        $turno->setTipo1($tipoBox_var->getLetra()."");
        }
        else{
          $turno->setNumero1('');
        $turno->setBox1('');
        $turno->setTipo1('');
        }



        

        

        

        

        $turno->setNumero(1);
        $turno->setBox($box);
        $turno->setTipo($turno->getTipo());

        $em = $this->getDoctrine()->getManager();
        $em->persist($turno);
        $em->flush();

        $array = array(
            'box' => $turno->getBox()->getNumero(), //. ' - ' . $entity->getTipo(),
            'turno' => $turno->getNumero(),
            'tipo' => $turno->getTipo(),

            'box1' => $turno->getBox1() . ' - ' . $turno->getTipo1(),
            'turno1' => $turno->getNumero1(),
          //  'tipo1' => $entity->getTipo1(),


            'box2' => $turno->getBox2() . ' - ' . $turno->getTipo2(),
            'turno2' => $turno->getNumero2(),
       //     'tipo2' => $entity->getTipo2(),

            'box3' => $turno->getBox3(). ' - ' . $turno->getTipo3(),
            'turno3' => $turno->getNumero3(),
     //       'tipo3' => $entity->getTipo3(),

            'box4' => $turno->getBox4(). ' - ' . $turno->getTipo4(),
            'turno4' => $turno->getNumero4(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;
        
        }

    /**
     * Lists all Turno entities.
     *
     * @Route("/", name="turno")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Turno')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Turno entity.
     *
     * @Route("/", name="turno_create")
     * @Method("POST")
     * @Template("IPDUVTurnadorBundle:Turno:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Turno();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('turno_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Turno entity.
     *
     * @param Turno $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Turno $entity)
    {
        $form = $this->createForm(new TurnoType(), $entity, array(
            'action' => $this->generateUrl('turno_create'),
            'method' => 'POST',
        ));

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Box')->findAll();


        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Turno entity.
     *
     * @Route("/new", name="turno_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Turno();
        $form   = $this->createCreateForm($entity);

        $em = $this->getDoctrine()->getManager();
        $tipoBox = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->findAll();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Box')->findAll();
        
        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'tipo_box' => $tipoBox,
            'boxs' => $entities,
        );
    }

    /**
     * Finds and displays a Turno entity.
     *
     * @Route("/{id}", name="turno_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Turno entity.
     *
     * @Route("/{id}/edit", name="turno_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Turno entity.
    *
    * @param Turno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Turno $entity)
    {
        $form = $this->createForm(new TurnoType(), $entity, array(
            'action' => $this->generateUrl('turno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));  
        $form->add('ajax_id', 'hidden', array('data' => $entity->getId()));

        return $form;
    }
    /**
     * Edits an existing Turno entity.
     *
     * @Route("/{id}", name="turno_update")
     * @Method("PUT")
     * @Template("IPDUVTurnadorBundle:Turno:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('turno_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Turno entity.
     *
     * @Route("/{id}", name="turno_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Turno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('turno'));
    }

    /**
     * Creates a form to delete a Turno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('turno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }



    //EL POST AJAX ANTES DE LA BOLUDEZ DE SAMUEL
    public function postAjaxAct1ion() {
        $request = $this->getRequest();
        $entity = new Turno();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        //var_dump($entity);
        //die;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }
    
    /**
     * @Route("/editaj1ax/", name="edi1t_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAc11tion(Request $request) {

  /*       $session = $request->getSession();
 
    // guarda un atributo para reutilizarlo durante una
    // petición posterior del usuario
   // $session->set('foo', 'bar');
 
    // obtener el valor de un atributo de la sesión
    $foo = $session->get('foo');



  //  foreach ($entities as $e) {
         $ho = array(
                           // 'Titulo' => 'El beneficiario ya se encuentra en los siguientes Expedientes: ',
                            'Mensaje' =>  'HOLA' ,//. $e['exp2'] . $e['exp3'] . $e['exp4'],
                            'Id' => '123' ,
                          );
       // $array[] = $ho;
   // }
      //   $ho = 'hola';
     $session->set('foo', $ho);

    $foo = $session->get('foo');
  throw $this->createNotFoundException($foo['Mensaje']);*/
   // var_dump($foo->Mensaje);die;
    // utilizar un valor por defecto si el atributo no existe
   // $filters = $session->get('filters', array());

        $entity = new Turno();
        $resultado = false;

                $em = $this->getDoctrine()->getManager();

                $hola = $request->request->get('tipo-box');
    //    var_dump($hola); die;
              //  $data = $request->request->get('request');
//        $idTipo =  $request->request->get('hola');
 //$title   =  $this->get('request')->request->all();

 //$this->get('request')->request->get('tipo-box');
      //  var_dump($title); die;
        $tipoBox_var = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($hola);
       // var_dump($tipoBox_var->getDescripcion()); die;

        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getId());
        $box->setTipoBox($tipoBox_var);

        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);

        


        //$box->setTipoBox()
        


        
//        $turno->setBox1($em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getNumero()));


        

        $turno->setNumero4($turno->getNumero3());
        $turno->setBox4($turno->getBox3()."");
        $turno->setTipo4($turno->getTipo3()."");

        $turno->setNumero3($turno->getNumero2());
        $turno->setBox3($turno->getBox2()."");
        $turno->setTipo3($turno->getTipo2()."");

        $turno->setNumero2($turno->getNumero1());
        $turno->setBox2($turno->getBox1()."");
        $turno->setTipo2($turno->getTipo1()."");

        $turno->setNumero1($turno->getNumero());
        $turno->setBox1($turno->getBox()->getNumero());
        $turno->setTipo1($turno->getBox()->getTipoBox()->getLetra());



        $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getId());

        $tipo = $box->getTipoBox();


        $turno->setBox($box);

        // AUMENTO EL TURNO EN 1
       // $turno->setAnterior($turno->getTurno());
       // $turno->setBoxAnterior($turno->getBox());
        $numero_turno = $turno->getNumero() + 1;
        $turno->setNumero($numero_turno);
        // ASIGNO EL BOX QUE RECUPERE DEL FORMULARIO
       


     //   $box = new Box();
       // $box = $this->getUser()->getBox()->getId();


      //  $tipo = $em->getRepository('IPDUVTurnadorBundle:Box')->find($this->getUser()->getBox()->getId());

// throw $this->createNotFoundException($this->getUser()->getBox()->getId());

  //     $user = $this->container->get('security.context')->getToken()->getUser();
//$user->getId();

// $repository = $this->getDoctrine()
//     ->getRepository('IPDUVTurnadorBundle:Box');

// $query = $repository->createQueryBuilder('p')
//     ->where('p.usuario = :price')
//     ->setParameter('price', $user->getId())
//     ->getQuery();

//$box = new Box();


//$repository->find($id);
 
//$box->findOneByUsuarioId($user->getId);

//$usr= $this->get('security.context')->getToken()->getUser();
//$usr->getUsername();

//$box = $query->getResult();

    //    var_dump($user);die;
       // throw $this->createNotFoundException($box[id]);
       // $box = $em->getRepository('IPDUVTurnadorBundle:Box')->find($user->getBox()->getNumero());
     
       // $turno->setBox(1);
        
        
      //  var_dump($turno); die;
//        if ($form->isValid()) {
            if($numero_turno >= 100)
            {
                $turno->setNumero(0);
                $turno->setBox1(0);
                $turno->setNumero1(0);
                $turno->setBox2(0);
                $turno->setNumero2(0);
                $turno->setBox3(0);
                $turno->setNumero3(0);
                $turno->setBox4(0);
                $turno->setNumero4(0);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($turno);
            $em->flush();
            
  //      }
        $array = array(
            'turno' => $numero_turno,
            'box' => $box->getNumero() . ' - ' . $box->getTipoBox()->getLetra(),
            
            'box1' => $turno->getBox1(),
            'turno1' => $turno->getNumero1() . ' - ' . $turno->getTipo1(),
//            'tipo1' => $turno->getTipo1(),

            'box2' => $turno->getBox2(),
            'turno2' => $turno->getNumero2() . ' - ' . $turno->getTipo2(),
         //   'tipo2' => $turno->getTipo2(),

            'box3' => $turno->getBox3(),
            'turno3' => $turno->getNumero3() . ' - ' . $turno->getTipo3(),
         //   'tipo3' => $turno->getTipo3(),

            'box4' => $turno->getBox4(),
            'turno4' => $turno->getNumero4() . ' - ' . $turno->getTipo4(),
           // 'tipo4' => $turno->getTipo4(),
          //  'box' => $turno->getLetra(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;
        
        }
}
