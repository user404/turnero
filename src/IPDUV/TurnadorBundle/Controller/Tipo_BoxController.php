<?php

namespace IPDUV\TurnadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TurnadorBundle\Entity\Tipo_Box;
use IPDUV\TurnadorBundle\Form\Tipo_BoxType;

/**
 * Tipo_Box controller.
 *
 * @Route("/tipo_box")
 */
class Tipo_BoxController extends Controller
{

    /**
     * Lists all Tipo_Box entities.
     *
     * @Route("/", name="tipo_box")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Tipo_Box entity.
     *
     * @Route("/", name="tipo_box_create")
     * @Method("POST")
     * @Template("IPDUVTurnadorBundle:Tipo_Box:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Tipo_Box();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipo_box_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Tipo_Box entity.
     *
     * @param Tipo_Box $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tipo_Box $entity)
    {
        $form = $this->createForm(new Tipo_BoxType(), $entity, array(
            'action' => $this->generateUrl('tipo_box_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tipo_Box entity.
     *
     * @Route("/new", name="tipo_box_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Tipo_Box();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Tipo_Box entity.
     *
     * @Route("/{id}", name="tipo_box_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipo_Box entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Tipo_Box entity.
     *
     * @Route("/{id}/edit", name="tipo_box_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipo_Box entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Tipo_Box entity.
    *
    * @param Tipo_Box $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Tipo_Box $entity)
    {
        $form = $this->createForm(new Tipo_BoxType(), $entity, array(
            'action' => $this->generateUrl('tipo_box_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Tipo_Box entity.
     *
     * @Route("/{id}", name="tipo_box_update")
     * @Method("PUT")
     * @Template("IPDUVTurnadorBundle:Tipo_Box:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipo_Box entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('tipo_box_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Tipo_Box entity.
     *
     * @Route("/{id}", name="tipo_box_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTurnadorBundle:Tipo_Box')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tipo_Box entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipo_box'));
    }

    /**
     * Creates a form to delete a Tipo_Box entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipo_box_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
