<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Turno
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Turno
{

    /**
     * @var string
     *
     */
    private $ajax_id;
    
    /**
     *@ORM\ManyToOne(targetEntity="Box", inversedBy="turno", cascade={"persist", "remove"})
     *@ORM\JoinColumn(name="box", referencedColumnName="id")
     */
    private $box;

    /**
     * Set ajax_id
     *
     * @param integer $ajax_id
     */
    public function setAjaxId($ajax_id)
    {
        $this->ajax_id = $ajax_id;

        return $this;
    }

    /**
     * Get ajax_id
     *
     * @return integer 
     */
    public function getAjaxId()
    {
        return $this->ajax_id;
    }
    

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *ORM\OneToMany(targetEntity="Consulta", mappedBy="turno")
    **/
    private $consultas;

    /**
     * @ORM\ManyToOne(targetEntity="\ALIAS\UserBundle\Entity\Area", inversedBy="turno")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    protected $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;


    /**
     * @var integer
     *
     * @ORM\Column(name="numero1", type="string", length=3, nullable=true)
     */
    private $numero1;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero2", type="string", length=3, nullable=true)
     */
    private $numero2;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero3", type="string", length=3, nullable=true)
     */
    private $numero3;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero4", type="string", length=3, nullable=true)
     */
    private $numero4;

    /**
     * @var integer
     *
     * @ORM\Column(name="box1", type="string", length=3, nullable=true)
     */
    private $box1;

    /**
     * @var integer
     *
     * @ORM\Column(name="box2", type="string", length=3, nullable=true)
     */
    private $box2;

    /**
     * @var integer
     *
     * @ORM\Column(name="box3", type="string", length=3, nullable=true)
     */
    private $box3;

    /**
     * @var integer
     *
     * @ORM\Column(name="box4", type="string", length=3, nullable=true)
     */
    private $box4;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo", type="string", length=3, nullable=true)
     */
    private $tipo;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo1", type="string", length=3, nullable=true)
     */
    private $tipo1;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo2", type="string", length=3, nullable=true)
     */
    private $tipo2;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo3", type="string", length=3, nullable=true)
     */
    private $tipo3;

    /**
     * @var integer
     *
     * @ORM\Column(name="tipo4", type="string", length=3, nullable=true)
     */
    private $tipo4;



    /**
     * Set numero
     *
     * @param integer $numero
     * @return Turno
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set box
     *
     * @param \IPDUV\TurnadorBundle\Entity\Box $box
     * @return Turno
     */
    public function setBox(\IPDUV\TurnadorBundle\Entity\Box $box = null)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return \IPDUV\TurnadorBundle\Entity\Box 
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set area
     *
     * @param \ALIAS\UserBundle\Entity\Area $area
     *
     * @return Turno
     */
    public function setArea(\ALIAS\UserBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \ALIAS\UserBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }



    /**
     * Set numero1
     *
     * @param string $numero1
     *
     * @return Turno
     */
    public function setNumero1($numero1)
    {
        $this->numero1 = $numero1;

        return $this;
    }

    /**
     * Get numero1
     *
     * @return string
     */
    public function getNumero1()
    {
        return $this->numero1;
    }

    /**
     * Set numero2
     *
     * @param string $numero2
     *
     * @return Turno
     */
    public function setNumero2($numero2)
    {
        $this->numero2 = $numero2;

        return $this;
    }

    /**
     * Get numero2
     *
     * @return string
     */
    public function getNumero2()
    {
        return $this->numero2;
    }

    /**
     * Set numero3
     *
     * @param string $numero3
     *
     * @return Turno
     */
    public function setNumero3($numero3)
    {
        $this->numero3 = $numero3;

        return $this;
    }

    /**
     * Get numero3
     *
     * @return string
     */
    public function getNumero3()
    {
        return $this->numero3;
    }

    /**
     * Set numero4
     *
     * @param string $numero4
     *
     * @return Turno
     */
    public function setNumero4($numero4)
    {
        $this->numero4 = $numero4;

        return $this;
    }

    /**
     * Get numero4
     *
     * @return string
     */
    public function getNumero4()
    {
        return $this->numero4;
    }

    /**
     * Set box1
     *
     * @param string $box1
     *
     * @return Turno
     */
    public function setBox1($box1)
    {
        $this->box1 = $box1;

        return $this;
    }

    /**
     * Get box1
     *
     * @return string
     */
    public function getBox1()
    {
        return $this->box1;
    }

    /**
     * Set box2
     *
     * @param string $box2
     *
     * @return Turno
     */
    public function setBox2($box2)
    {
        $this->box2 = $box2;

        return $this;
    }

    /**
     * Get box2
     *
     * @return string
     */
    public function getBox2()
    {
        return $this->box2;
    }

    /**
     * Set box3
     *
     * @param string $box3
     *
     * @return Turno
     */
    public function setBox3($box3)
    {
        $this->box3 = $box3;

        return $this;
    }

    /**
     * Get box3
     *
     * @return string
     */
    public function getBox3()
    {
        return $this->box3;
    }

    /**
     * Set box4
     *
     * @param string $box4
     *
     * @return Turno
     */
    public function setBox4($box4)
    {
        $this->box4 = $box4;

        return $this;
    }

    /**
     * Get box4
     *
     * @return string
     */
    public function getBox4()
    {
        return $this->box4;
    }

    /**
     * Set tipo1
     *
     * @param string $tipo1
     *
     * @return Turno
     */
    public function setTipo1($tipo1)
    {
        $this->tipo1 = $tipo1;

        return $this;
    }

    /**
     * Get tipo1
     *
     * @return string
     */
    public function getTipo1()
    {
        return $this->tipo1;
    }

    /**
     * Set tipo2
     *
     * @param string $tipo2
     *
     * @return Turno
     */
    public function setTipo2($tipo2)
    {
        $this->tipo2 = $tipo2;

        return $this;
    }

    /**
     * Get tipo2
     *
     * @return string
     */
    public function getTipo2()
    {
        return $this->tipo2;
    }

    /**
     * Set tipo3
     *
     * @param string $tipo3
     *
     * @return Turno
     */
    public function setTipo3($tipo3)
    {
        $this->tipo3 = $tipo3;

        return $this;
    }

    /**
     * Get tipo3
     *
     * @return string
     */
    public function getTipo3()
    {
        return $this->tipo3;
    }

    /**
     * Set tipo4
     *
     * @param string $tipo4
     *
     * @return Turno
     */
    public function setTipo4($tipo4)
    {
        $this->tipo4 = $tipo4;

        return $this;
    }

    /**
     * Get tipo4
     *
     * @return string
     */
    public function getTipo4()
    {
        return $this->tipo4;
    }


    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Turno
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
