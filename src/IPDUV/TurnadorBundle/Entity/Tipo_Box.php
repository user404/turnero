<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tipo_Box
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tipo_Box
{

    /**
    *ORM\OneToMany(targetEntity="Box", mappedBy="tipo_box")
    **/
    private $box;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="letra", type="string", length=2)
     */
    private $letra;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50)
     */
    private $descripcion;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set letra
     *
     * @param string $letra
     *
     * @return Tipo_Box
     */
    public function setLetra($letra)
    {
        $this->letra = $letra;

        return $this;
    }

    /**
     * Get letra
     *
     * @return string
     */
    public function getLetra()
    {
        return $this->letra;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tipo_Box
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
