<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Box
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Box
{
    /**
     *@ORM\ManyToOne(targetEntity="Tipo_Box", inversedBy="box", cascade={"persist", "remove"})
     *@ORM\JoinColumn(name="tipo_box_id", referencedColumnName="id")
     */
    private $tipo_box;


    // POR AHORA ESTA N-N PERO SEGURAMENTE VA A TERMINAR SIENDO 1-1


    /**
     * @ORM\OneToOne(targetEntity="\ALIAS\UserBundle\Entity\User", inversedBy="box")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id")
     */
    private $usuario;

    /**
    *ORM\OneToMany(targetEntity="Turno", mappedBy="box")
    **/
    private $turno;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=2, nullable=true)
     */
    private $tipo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return Box
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }


    /**
     * Set usuario
     *
     * @param \ALIAS\UserBundle\Entity\User $usuario
     *
     * @return Box
     */
    public function setUsuario(\ALIAS\UserBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \ALIAS\UserBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Box
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }


    /**
     * Set tipoBox
     *
     * @param \IPDUV\TurnadorBundle\Entity\Tipo_Box $tipoBox
     *
     * @return Box
     */
    public function setTipoBox(\IPDUV\TurnadorBundle\Entity\Tipo_Box $tipoBox = null)
    {
        $this->tipo_box = $tipoBox;

        return $this;
    }

    /**
     * Get tipoBox
     *
     * @return \IPDUV\TurnadorBundle\Entity\Tipo_Box
     */
    public function getTipoBox()
    {
        return $this->tipo_box;
    }
}
