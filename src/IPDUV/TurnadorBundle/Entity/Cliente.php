<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cliente
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var string
     *
     */
    private $ajax_id;


    /**
     * Set ajax_id
     *
     * @param integer $ajax_id
     */
    public function setAjaxId($ajax_id)
    {
        $this->ajax_id = $ajax_id;

        return $this;
    }

    /**
     * Get ajax_id
     *
     * @return integer 
     */
    public function getAjaxId()
    {
        return $this->ajax_id;
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *@ORM\OneToMany(targetEntity="Consulta", mappedBy="cliente", cascade={"all"})
    **/
    private $consultas;

     public function __construct()
    {
        $this->consultas = new ArrayCollection();
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=20, nullable=true)
     */
    private $dni;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=20, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="correo", type="string", length=40, nullable=true)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=50, nullable=true)
     */
    private $direccion;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellido
     *
     * @param string $apellido
     * @return Cliente
     */
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get apellido
     *
     * @return string 
     */
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set dni
     *
     * @param string $dni
     * @return Cliente
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set correo
     *
     * @param string $correo
     * @return Cliente
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string 
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Cliente
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add consultas
     *
     * @param \IPDUV\TurnadorBundle\Entity\Consulta $consultas
     * @return Cliente
     */
    public function addConsulta(\IPDUV\TurnadorBundle\Entity\Consulta $consultas)
    {
        $consultas->setCliente($this);
        $this->consultas[] = $consultas;

        return $this;
    }
    
    /**
     * Remove consultas
     *
     * @param \IPDUV\TurnadorBundle\Entity\Consulta $consultas
     */
    public function removeConsulta(\IPDUV\TurnadorBundle\Entity\Consulta $consultas)
    {
        $this->consultas->removeElement($consultas);
    }

    /**
     * Get consultas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConsultas()
    {
        return $this->consultas;
    }
}
