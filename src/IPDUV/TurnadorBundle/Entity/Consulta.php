<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consulta
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Consulta
{
    /**
     * @var string
     *
     */
    private $ajax_id;


    /**
     * Set ajax_id
     *
     * @param integer $ajax_id
     */
    public function setAjaxId($ajax_id)
    {
        $this->ajax_id = $ajax_id;

        return $this;
    }

    /**
     * Get ajax_id
     *
     * @return integer 
     */
    public function getAjaxId()
    {
        return $this->ajax_id;
    }
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *@ORM\ManyToOne(targetEntity="Turno", inversedBy="consultas")
     *@ORM\JoinColumn(name="turno_id", referencedColumnName="id")
     */
    private $turno;

    /**
     *@ORM\ManyToOne(targetEntity="Cliente", inversedBy="consultas")
     *@ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     */
    private $cliente;

    /**
     * @ORM\ManyToOne(targetEntity="\ALIAS\UserBundle\Entity\Area", inversedBy="consultas")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    protected $area;

    /**
     * @ORM\ManyToOne(targetEntity="\ALIAS\UserBundle\Entity\User", inversedBy="generada")
     * @ORM\JoinColumn(name="creador_id", referencedColumnName="id")
     */
    protected $creador;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="string", length=255, nullable=true)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="asunto", type="string", length=255, nullable=true)
     */
    private $asunto;

    /**
     * @var string
     *
     * @ORM\Column(name="detalle", type="string", length=255, nullable=true)
     */
    private $detalle;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set asunto
     *
     * @param string $asunto
     * @return Consulta
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    /**
     * Get asunto
     *
     * @return string 
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set turno
     *
     * @param \IPDUV\TurnadorBundle\Entity\Turno $turno
     * @return Consulta
     */
    public function setTurno(\IPDUV\TurnadorBundle\Entity\Turno $turno = null)
    {
        $this->turno = $turno;

        return $this;
    }

    /**
     * Get turno
     *
     * @return \IPDUV\TurnadorBundle\Entity\Turno 
     */
    public function getTurno()
    {
        return $this->turno;
    }

    /**
     * Set cliente
     *
     * @param \IPDUV\TurnadorBundle\Entity\Cliente $cliente
     * @return Consulta
     */
    public function setCliente(\IPDUV\TurnadorBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \IPDUV\TurnadorBundle\Entity\Cliente 
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * Set fecha
     *
     * @param string $fecha
     * @return Consulta
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return string 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Consulta
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set area
     *
     * @param \ALIAS\UserBundle\Entity\Area $area
     *
     * @return Consulta
     */
    public function setArea(\ALIAS\UserBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \ALIAS\UserBundle\Entity\Area
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set creador
     *
     * @param \ALIAS\UserBundle\Entity\User $creador
     *
     * @return Consulta
     */
    public function setCreador(\ALIAS\UserBundle\Entity\User $creador = null)
    {
        $this->creador = $creador;

        return $this;
    }

    /**
     * Get creador
     *
     * @return \ALIAS\UserBundle\Entity\User
     */
    public function getCreador()
    {
        return $this->creador;
    }

}
