<?php

namespace IPDUV\TurnadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TurnoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', 'text', array( 'label'=>'Numero:','required'=>false,'attr' => array('class'=>'form-control')))
        //    ->add('letra', 'text', array( 'label'=>'Letra:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('area',"entity",array('label'=>'Area:','class'=>'ALIASUserBundle:Area', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TurnadorBundle\Entity\Turno'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_turnadorbundle_turno';
    }
}
