<?php

namespace IPDUV\TurnadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConsultaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fecha', 'text', array( 'label'=>'Fecha:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('asunto', 'text', array( 'label'=>'Asunto:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('detalle', 'textarea', array( 'label'=>'Detalle:','required'=>false,'attr' => array('class'=>'form-control')))
//          ->add('turno',"entity",array('label'=>'Turno:','class'=>'IPDUVTurnadorBundle:Turno', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
//            ->add('cliente',"entity",array('label'=>'Cliente:','class'=>'IPDUVTurnadorBundle:Cliente', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
//            ->add('area',"entity",array('label'=>'Area:','class'=>'IPDUVUserBundle:Area', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TurnadorBundle\Entity\Consulta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_turnadorbundle_consulta';
    }
}
