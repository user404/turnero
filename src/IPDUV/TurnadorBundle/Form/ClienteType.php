<?php

namespace IPDUV\TurnadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClienteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nombre', 'text', array( 'label'=>'Nombre:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('apellido', 'text', array( 'label'=>'Apellido:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('dni', 'text', array( 'label'=>'DNI:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('telefono', 'text', array( 'label'=>'Telefono:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('correo', 'text', array( 'label'=>'Correo:','required'=>false,'attr' => array('class'=>'form-control')))
            ->add('direccion', 'text', array( 'label'=>'Direccion:','required'=>false,'attr' => array('class'=>'form-control')))
            ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TurnadorBundle\Entity\Cliente'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_turnadorbundle_cliente';
    }
}
