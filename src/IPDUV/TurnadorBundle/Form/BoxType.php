<?php

namespace IPDUV\TurnadorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BoxType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('usuario',"entity",array('label'=>'Usuario:','class'=>'ALIASUserBundle:User',
                'property'=>'username','required'=>false,'attr' => array('class'=>'form-control') ))
            ->add('tipo_box',"entity",array('label'=>'Tipo:','class'=>'IPDUVTurnadorBundle:Tipo_Box',
                'property'=>'descripcion','required'=>false,'attr' => array('class'=>'form-control') ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IPDUV\TurnadorBundle\Entity\Box'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ipduv_turnadorbundle_box';
    }
}
