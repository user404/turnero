<?php

namespace IPDUV\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *@ORM\OneToOne(targetEntity="\IPDUV\TurnadorBundle\Entity\Box", mappedBy="usuario")
     */
    private $box;
    
    /**
    *ORM\OneToMany(targetEntity="\IPDUV\TurnadorBundle\Entity\Consulta", mappedBy="creador")
    **/
    private $generada;

     /**
     *@ORM\ManyToOne(targetEntity="Area", inversedBy="usuarios")
     *@ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    // public function __construct()
    // {
    //     parent::__construct();
    //     // your own logic
    // }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set area
     *
     * @param \IPDUV\UserBundle\Entity\Area $area
     * @return User
     */
    public function setArea(\IPDUV\UserBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \IPDUV\UserBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }


    /**
     * Set box
     *
     * @param \IPDUV\TurnadorBundle\Entity\Box $box
     * @return User
     */
    public function setBox(\IPDUV\TurnadorBundle\Entity\Box $box = null)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return \IPDUV\TurnadorBundle\Entity\Box 
     */
    public function getBox()
    {
        return $this->box;
    }
}
