<?php

namespace IPDUV\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IPDUVUserBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}
