<?php

namespace IPDUV\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        // add your custom field
        $builder
        ->add('box', 'text', array( 'label'=>'Box:','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('username', 'text', array( 'label'=>'Nombre de usuario:','required'=>false,'attr' => array('class'=>'form-control')))
        ->add('email', 'text', array( 'label'=>'form.email', 'translation_domain' => 'FOSUserBundle' ,'required'=>false,'attr' => array('class'=>'form-control')))
        ->add('plainPassword', 'repeated', array(
            'type' => 'password',
            'options' => array('translation_domain' => 'FOSUserBundle'),
            'first_options' => array('label' => 'form.password', 'attr' => array('class'=>'form-control')),
            'second_options' => array('label' => 'form.password_confirmation', 'attr' => array('class'=>'form-control')),
            'invalid_message' => 'fos_user.password.mismatch',

        ))
        ->add('area',"entity",array('label'=>'Area:','class'=>'IPDUVUserBundle:Area', 'property'=>'nombre','required'=>false,'attr' => array('class'=>'form-control') ))
        ;
    }

    public function getName()
    {
        return 'ipduv_user_registration';
    }
}