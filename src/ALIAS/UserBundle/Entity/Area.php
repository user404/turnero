<?php

namespace ALIAS\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Area
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Area
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *ORM\OneToMany(targetEntity="\ALIAS\TurnadorBundle\Entity\Consulta", mappedBy="area")
    **/
    private $consultas;

    /**
    *ORM\OneToMany(targetEntity="\ALIAS\TurnadorBundle\Entity\Turno", mappedBy="area")
    **/
    private $turno;


    /**
    *ORM\OneToMany(targetEntity="User", mappedBy="area")
    **/
    private $usuarios;    

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=25)
     */
    private $nombre;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Area
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }
}
