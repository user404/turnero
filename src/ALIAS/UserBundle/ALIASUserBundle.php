<?php

namespace ALIAS\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ALIASUserBundle extends Bundle
{
	public function getParent()
    {
        return 'FOSUserBundle';
    }
}
