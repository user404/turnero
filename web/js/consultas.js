var x;
x=$(document);
x.ready(inicializarEventos);

function inicializarEventos()
{
    listarConsultas();
    //nuevoBeneficiario()
    //editarBeneficiario();

}

function listarConsultas()
{ 
    var elemento = $('#table_id thead th');
    var cont = elemento.length - 2;
    var id = $("#cliente_id1").val();
    var table = $('#table_id').DataTable({
        "ajax" : Routing.generate('traer_consultas', { id: id }),
         "order": [[ 0, "asc" ]],
         "language": {
         "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"},
          "columns": [
                { "data": "Id", "visible": false},
                { "data": "Fecha", "visible": true},
                { "data": "Asunto" , "visible": true},
                { "data": "Detalle" , "visible": true},
                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                        var url = "{{ path('consulta_edit',{'id': 'IDPROG'}) }}";

                  //  return "<a class='btn btn-primary' href='" + urldef+ "''> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalEditarBeneficiario'data-whatever='"+data.Id+"'  data-whatever='"+data.baja+"'><i class='glyphicon glyphicon-pencil'></i></button>"

                    ;}

        
                },

                {
                    "data": null,
                    "render": function ( data, type, full, meta ) {
                       // var url = "{{ path('beneficiario_edit',{'id': 'IDPROG'}) }}";

                  //  return "<a class='btn btn-primary' href='" + urldef+ "''> <span class='glyphicon glyphicon-pencil'></span>"
                  return "<button id='btnB' type='button' class='btn btn-warning' data-toggle='modal' data-target='#modalBorrarBeneficiario'data-whatever='"+data.Id+"' data-baja='"+data.baja+"'><i class='glyphicon glyphicon-remove'></i></button>"

                    ;}

        
                }

        ], 
    });
 
}

function recargarBeneficiario(data){
    debugger;
    var table = $('#table_id').DataTable();
    table.ajax.url(Routing.generate('traer_consultas', { id: $("#cliente_id1").val()})).load();
    //$('#myModal').modal('hide');
}


// $('#modalBorrarBeneficiario').on('shown.bs.modal', function (event) {
//     debugger;
  
//   var button = $(event.relatedTarget) // Button that triggered the modal
//   var recipient = button.data('whatever') // Extract info from data-* attributes
//   // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   var recipient1 = button.data('baja')

//   var modal = $(this)
//   //modal.find('.modal-title').text('New message to ' + recipient)
//   modal.find('#idEliminarBeneficiario').val(recipient)


  

// })


function nuevaConsulta(){
  
  $("#frmNuevoBeneficiario").on('submit',(function(e) {
    debugger;
    e.preventDefault();
    $.ajax({
    url: Routing.generate('post_ajax_consulta', { id: $("#cliente_id").val()}), //"ajax_php_file.php", // Url to which the request is sendY
    type: "POST",             // Type of request to be send, called as method
    data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {

        $("#msjAgregarConsulta").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
        $("#msjAgregarConsulta").fadeIn(1000);
        $("#msjAgregarConsulta").fadeOut(2000, function(){
         $('#modalAgregarConsulta').modal('hide');
         $('#id').val("");
         $('#nombre').val("");
         $('#apellido').val("");
         $('#documento').val("");
         $('#telefono').val("");
         $('#direccion').val("");
         recargarBeneficiario();
       });            
      }
    });
}));
//}

// //function editarBeneficiario(){
//   //debugger;
// $('#modalEditarBeneficiario').on('shown.bs.modal', function (event) {
//   debugger;
//    var button = $(event.relatedTarget); // Button that triggered the modal
//    var recipient = button.data('whatever'); // Extract info from data-* attributes
//    // var recipient1 = button.data('baja'); // Extract info from data-* attributes

//    $.ajax({
//       url: Routing.generate('unbene', { id: recipient }), //"ajax_php_file.php", // Url to which the request is sendY
//       type: "GET",             // Type of request to be send, called as method
//       //data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
//       contentType: false,       // The content type used when sending data to the server.
//       cache: false,             // To unable request pages to be cached
//       processData:false,        // To send DOMDocument or non processed data file it is set to false
//       success: function(data)   // A function to be called if request succeeds
//       {
//          var div = $('#modalEditarBeneficiario');
//          div.find(':input[id^="beneficiarioId"]').val(data.Id);
//         div.find(':input[id^="documento"]').val(data.Documento);
//         div.find(':input[id^="nombre"]').val(data.Nombre);
//         div.find(':input[id^="apellido"]').val(data.Apellido);
//         div.find(':input[id^="email"]').val(data.Email);
//         div.find(':input[id^="telefono"]').val(data.Telefono);
//         div.find(':input[id^="direccion"]').val(data.Direccion);
//         //div.find(':input[id^="mz"]').val(data.Mz); 
//         //div.find(':input[id^="pc"]').val(data.Pc);

//         $("#beneficiarioId").val(data.Id)
//       }
//       });
// })


// $("#editarBeneficiario").on('submit',(function(e) {
//     debugger;
//      var div = $('#modalEditarBeneficiario');
//      var id_bene = div.find(':input[id^="beneficiarioId"]').val();

//                     e.preventDefault();                    
//                     $.ajax({
//                     url: Routing.generate('edit_ajax_beneficiario', { id: id_bene}), //"ajax_php_file.php", // Url to which the request is sendY
//                     type: "POST",             // Type of request to be send, called as method
//                     data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
//                     contentType: false,       // The content type used when sending data to the server.
//                     cache: false,             // To unable request pages to be cached
//                     processData:false,        // To send DOMDocument or non processed data file it is set to false
//                     success: function(data)   // A function to be called if request succeeds
//                     {

//              //          var div = $('#modalEditarObservacion');
//              // div.find(':input[id^="observacion_id"]').val(data.Id);
//              // div.find(':input[id^="fecha"]').val(data.Fecha);
//              // div.find(':input[id^="tipo"]').val(data.Tipo);
//              // div.find(':input[id^="observacion"]').val(data.Observacion);

//                                                // debugger;
//                         //  $('#id').val(data.Id);
//                         //  $('#fecha').val(data.Fecha);
//                         //  $('#tipo').val(data.Tipo);
//                         //  $('#observacion').val(data.Observacion);
//                         $("#msjEditarBeneficiario").html("<div class='alert alert-success' role='alert'>"+data.Mensaje+"</div>");
//                         $("#msjEditarBeneficiario").fadeIn(1000);
//                         $("#msjEditarBeneficiario").fadeOut(2000, function(){
//                              $('#modalEditarBeneficiario').modal('hide');
//                              $('#id').val("");
//                              $('#nombre').val("");
//                              $('#apellido').val("");
//                              $('#documento').val("");
//                              $('#telefono').val("");
//                              $('#direccion').val("");

//                              recargarBeneficiario();
//                         });
    
                         
//                     }
//                 });
//  }));
//}
